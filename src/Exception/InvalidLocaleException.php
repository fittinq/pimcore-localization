<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Localization\Exception;

use RuntimeException;

class InvalidLocaleException extends RuntimeException
{

}
